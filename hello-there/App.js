import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function App() {
  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 2 }} />
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <View
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            backgroundColor: "purple"
          }}
        >
          <Text style={{ fontWeight: "bold", color: "white" }}>Hello There!</Text>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }} />
      </View>
      <View style={{ flex: 2 }} />
    </View>
  );
}

const styles = StyleSheet.create({});
