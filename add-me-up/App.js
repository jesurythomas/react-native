import React, { Component } from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";

export default class Addition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNumber: "",
      secondNumber: "",
      sum: ""
    };
  }

  render() {
    const sum = this.state.sum;
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.numbers}>
          <TextInput
            style={{
              height: 40,
              width: 150,
              borderBottomColor: "black",
              borderBottomWidth: 1,
              textAlign: "center"
            }}
            placeholder="Enter First Number"
            onChangeText={(firstNumber) => this.setState({ firstNumber })}
            value={this.state.firstNumber}
            keyboardType={"numeric"}
          />
          <Text style={styles.plus}> + </Text>
          <TextInput
            style={{
              height: 40,
              width: 150,
              borderBottomColor: "black",
              borderBottomWidth: 1,
              textAlign: "center"
            }}
            placeholder="Enter Second Number"
            onChangeText={(secondNumber) => this.setState({ secondNumber })}
            value={this.state.secondNumber}
            keyboardType={"numeric"}
          />
          <Button style={styles.button} title="=" onPress={this.onAdd}></Button>
        </View>
        <View style={styles.container}>
          <Text style={styles.plus}>Sum: {sum}</Text>
        </View>
      </View>
    );
  }

  onAdd = () => {
    this.setState({
      sum: parseFloat(this.state.firstNumber) + parseFloat(this.state.secondNumber)
    });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  numbers: {
    flex: 1,
    backgroundColor: "white",
    color: "black",
    alignItems: "center",
    justifyContent: "center"
  },
  plus: {
    fontWeight: "bold",
    color: "black",
    fontSize: 25,
    height: 40,
    paddingTop: 10
  },
  button: {
    marginHorizontal: 16
  }
});
