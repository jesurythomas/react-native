import React, { Component } from "react";
import { Image, View, StyleSheet, Button } from "react-native";

export default class Lightbulb extends Component {
  constructor() {
    super();
    this.state = {
      currState: "0",
      uri: "https://images-na.ssl-images-amazon.com/images/I/513lLPH9c%2BL._SX466_.jpg",
      buttonState: "Turn On"
    };
  }

  lightOn = () => {
    this.setState({
      currState: "1",
      uri:
        "https://previews.123rf.com/images/murika/murika1511/murika151100069/48123160-bright-glowing-incandescent-light-bulb-on-a-white-background.jpg",
      buttonState: "Turn Off"
    });
  };

  lightOff = () => {
    this.setState({
      currState: "0",
      uri: "https://images-na.ssl-images-amazon.com/images/I/513lLPH9c%2BL._SX466_.jpg",
      buttonState: "Turn On"
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Image source={{ uri: this.state.uri }} style={{ width: 200, height: 200 }} />
        <Button
          title={this.state.buttonState}
          onPress={() => {
            let lightState = this.state.currState === "1" ? this.lightOff() : this.lightOn();
          }}
        ></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
