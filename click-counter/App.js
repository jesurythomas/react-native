import React, { Component } from "react";
import { StyleSheet, Text, View, Button } from "react-native";

class Clicker extends Component {
  state = {
    counter: 0
  };

  render() {
    const counter = this.state.counter;

    return (
      <View style={styles.container}>
        <Text style={styles.counter}>Counter: {counter}</Text>
        <Button title="Increment Number" onPress={this.onIncrement} />
      </View>
    );
  }

  onIncrement = () => {
    this.setState({
      counter: this.state.counter + 1
    });
  };
}

export default function App() {
  return <Clicker />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  counter: {
    fontSize: 25
  }
});
