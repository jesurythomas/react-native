import React, { Component } from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { CheckBox } from "react-native-elements";

export default class Styler extends Component {
  constructor() {
    super();
    this.state = {
      text: "",
      checkbox1: false,
      fontWeight: "normal",
      checkbox2: false,
      fontStyle: "normal",
      checkbox3: false,
      checkbox4: false,
      checkbox5: false,
      color: "black"
    };

    const textStyle = StyleSheet.create({});
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{
            height: 40,
            width: 150,
            borderBottomColor: "black",
            borderBottomWidth: 1,
            textAlign: "center"
          }}
          placeholder="Enter Text"
          onChangeText={(text) => this.setState({ text })}
          value={this.state.text}
        />
        <CheckBox
          title="Bold"
          checked={this.state.checkbox1}
          onPress={() => {
            this.setState({ checkbox1: !this.state.checkbox1 });
            let isBold =
              this.state.checkbox1 === true
                ? this.setState({ fontWeight: "normal" })
                : this.setState({ fontWeight: "bold" });
          }}
        />
        <CheckBox
          title="Italic"
          checked={this.state.checkbox2}
          onPress={() => {
            this.setState({ checkbox2: !this.state.checkbox2 });
            let isItalic =
              this.state.checkbox2 === true
                ? this.setState({ fontStyle: "normal" })
                : this.setState({ fontStyle: "italic" });
          }}
        />
        <CheckBox
          title="Red"
          checked={this.state.checkbox3}
          onPress={() => {
            this.setState({ checkbox3: true, checkbox4: false, checkbox5: false });
            this.setState({ color: "red" });
          }}
        />
        <CheckBox
          title="Green"
          checked={this.state.checkbox4}
          onPress={() => {
            this.setState({ checkbox3: false, checkbox4: true, checkbox5: false });
            this.setState({ color: "green" });
          }}
        />
        <CheckBox
          title="Blue"
          checked={this.state.checkbox5}
          onPress={() => {
            this.setState({ checkbox3: false, checkbox4: false, checkbox5: true });

            this.setState({ color: "blue" });
          }}
        />
        <Text
          style={{
            fontWeight: this.state.fontWeight,
            fontStyle: this.state.fontStyle,
            color: this.state.color,
            fontSize: 25
          }}
        >
          {" "}
          {this.state.text}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
